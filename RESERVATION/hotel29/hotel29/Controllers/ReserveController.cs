﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using hotel29.Models;

namespace hotel29.Controllers
{
    public class ReserveController : Controller
    {
        // GET: Reserve
        private  ReserveModel _db = new ReserveModel ();
        public ActionResult Index()
        {
            return View(_db.reserves .ToList());
        }
        public ActionResult Create()
        {
            return View();
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Create([Bind(Exclude = "Id")] reserve  r)
        {
            if (!ModelState.IsValid)
                return View();
            _db.reserves.Add(r);
            _db.SaveChanges();
            return View();

        }
        public ActionResult Edit(int id)
        {
            var editcust = (from res in _db.reserves where res.RId== id select res).First();
            return View(editcust);
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Edit(reserve r)
        {
            var Originalcust = _db.reserves .SingleOrDefault(res => res.RId == r.RId);
            if (Originalcust != null)
                Originalcust.check_in_date = r.check_in_date;
            Originalcust.check_out_date = r.check_out_date;
            Originalcust.Number_of_Guests = r.Number_of_Guests;
            Originalcust.Number_of_rooms = r.Number_of_rooms;
            Originalcust.credit_card = r.credit_card;
            Originalcust.name_on_credit_card = r.name_on_credit_card;
            Originalcust.credit_card_number = r.credit_card_number;
            Originalcust.Expiration_Date = r.Expiration_Date;
            _db.SaveChanges();
            return View();
        }

    }
}