namespace hotel29.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class ReserveModel : DbContext
    {
        public ReserveModel()
            : base("name=ReserveModel")
        {
        }

        public virtual DbSet<reserve> reserves { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<reserve>()
                .Property(e => e.check_in_date)
                .IsUnicode(false);

            modelBuilder.Entity<reserve>()
                .Property(e => e.check_out_date)
                .IsUnicode(false);

            modelBuilder.Entity<reserve>()
                .Property(e => e.Number_of_Guests)
                .IsUnicode(false);

            modelBuilder.Entity<reserve>()
                .Property(e => e.Number_of_rooms)
                .IsUnicode(false);

            modelBuilder.Entity<reserve>()
                .Property(e => e.credit_card)
                .IsUnicode(false);

            modelBuilder.Entity<reserve>()
                .Property(e => e.name_on_credit_card)
                .IsUnicode(false);

            modelBuilder.Entity<reserve>()
                .Property(e => e.credit_card_number)
                .IsUnicode(false);

            modelBuilder.Entity<reserve>()
                .Property(e => e.Expiration_Date)
                .IsUnicode(false);
        }
    }
}
