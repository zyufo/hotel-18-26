﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using test.Models;

namespace test.Controllers
{
    public class LoginController : Controller
    {
        protected override void HandleUnknownAction(string actionName)

        {

            try
            {

                this.View(actionName).ExecuteResult(this.ControllerContext);

            }
            catch (InvalidOperationException ieox)

            {

                ViewData["error"] = "Unknown Action: \"" + Server.HtmlEncode(actionName) + "\"";

                ViewData["exMessage"] = ieox.Message;

                this.View("Error").ExecuteResult(this.ControllerContext);

            }

        }
        // GET: Login
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Autherize(test.Models.User user)
        {
            using (Model1 _db = new Model1()) {
                var userdetails = _db.User.Where(x => x.UserName == user.UserName && x.Password == user.Password).FirstOrDefault();
                if (userdetails == null)
                {

                    return RedirectToAction("Error", "Login");
                }
                else return RedirectToAction("Autherize", "Login");
            }
                return View();
        }
        public ActionResult Signup() {
            return View();
        }
    }
}